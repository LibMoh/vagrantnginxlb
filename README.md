## Set up system ##

To create LoadBalancer with 2 Webservers run the following command on the terminal

```bash
vagrant up
```

## Testing LoadBalancer ##

To test that Load Balancer is functioning correctly run the testlb script.
```bash
./testlb
```
